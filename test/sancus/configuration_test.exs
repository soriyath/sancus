defmodule Sancus.ConfigurationTest do
  use Sancus.DataCase

  alias Sancus.Configuration

  describe "endpoints" do
    alias Sancus.Configuration.Endpoint

    @valid_attrs %{description: "some description", host: "some host", port: "some port"}
    @update_attrs %{description: "some updated description", host: "some updated host", port: "some updated port"}
    @invalid_attrs %{description: nil, host: nil, port: nil}

    def endpoint_fixture(attrs \\ %{}) do
      {:ok, endpoint} =
        attrs
        |> Enum.into(@valid_attrs)
        |> Configuration.create_endpoint()

      endpoint
    end

    test "list_endpoints/0 returns all endpoints" do
      endpoint = endpoint_fixture()
      assert Configuration.list_endpoints() == [endpoint]
    end

    test "get_endpoint!/1 returns the endpoint with given id" do
      endpoint = endpoint_fixture()
      assert Configuration.get_endpoint!(endpoint.id) == endpoint
    end

    test "create_endpoint/1 with valid data creates a endpoint" do
      assert {:ok, %Endpoint{} = endpoint} = Configuration.create_endpoint(@valid_attrs)
      assert endpoint.description == "some description"
      assert endpoint.host == "some host"
      assert endpoint.port == "some port"
    end

    test "create_endpoint/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Configuration.create_endpoint(@invalid_attrs)
    end

    test "update_endpoint/2 with valid data updates the endpoint" do
      endpoint = endpoint_fixture()
      assert {:ok, %Endpoint{} = endpoint} = Configuration.update_endpoint(endpoint, @update_attrs)
      assert endpoint.description == "some updated description"
      assert endpoint.host == "some updated host"
      assert endpoint.port == "some updated port"
    end

    test "update_endpoint/2 with invalid data returns error changeset" do
      endpoint = endpoint_fixture()
      assert {:error, %Ecto.Changeset{}} = Configuration.update_endpoint(endpoint, @invalid_attrs)
      assert endpoint == Configuration.get_endpoint!(endpoint.id)
    end

    test "delete_endpoint/1 deletes the endpoint" do
      endpoint = endpoint_fixture()
      assert {:ok, %Endpoint{}} = Configuration.delete_endpoint(endpoint)
      assert_raise Ecto.NoResultsError, fn -> Configuration.get_endpoint!(endpoint.id) end
    end

    test "change_endpoint/1 returns a endpoint changeset" do
      endpoint = endpoint_fixture()
      assert %Ecto.Changeset{} = Configuration.change_endpoint(endpoint)
    end
  end

  describe "warning_periods" do
    alias Sancus.Configuration.Warning_Period

    @valid_attrs %{days: 42}
    @update_attrs %{days: 43}
    @invalid_attrs %{days: nil}

    def warning__period_fixture(attrs \\ %{}) do
      {:ok, warning__period} =
        attrs
        |> Enum.into(@valid_attrs)
        |> Configuration.create_warning__period()

      warning__period
    end

    test "list_warning_periods/0 returns all warning_periods" do
      warning__period = warning__period_fixture()
      assert Configuration.list_warning_periods() == [warning__period]
    end

    test "get_warning__period!/1 returns the warning__period with given id" do
      warning__period = warning__period_fixture()
      assert Configuration.get_warning__period!(warning__period.id) == warning__period
    end

    test "create_warning__period/1 with valid data creates a warning__period" do
      assert {:ok, %Warning_Period{} = warning__period} = Configuration.create_warning__period(@valid_attrs)
      assert warning__period.days == 42
    end

    test "create_warning__period/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Configuration.create_warning__period(@invalid_attrs)
    end

    test "update_warning__period/2 with valid data updates the warning__period" do
      warning__period = warning__period_fixture()
      assert {:ok, %Warning_Period{} = warning__period} = Configuration.update_warning__period(warning__period, @update_attrs)
      assert warning__period.days == 43
    end

    test "update_warning__period/2 with invalid data returns error changeset" do
      warning__period = warning__period_fixture()
      assert {:error, %Ecto.Changeset{}} = Configuration.update_warning__period(warning__period, @invalid_attrs)
      assert warning__period == Configuration.get_warning__period!(warning__period.id)
    end

    test "delete_warning__period/1 deletes the warning__period" do
      warning__period = warning__period_fixture()
      assert {:ok, %Warning_Period{}} = Configuration.delete_warning__period(warning__period)
      assert_raise Ecto.NoResultsError, fn -> Configuration.get_warning__period!(warning__period.id) end
    end

    test "change_warning__period/1 returns a warning__period changeset" do
      warning__period = warning__period_fixture()
      assert %Ecto.Changeset{} = Configuration.change_warning__period(warning__period)
    end
  end

  describe "notification_channels" do
    alias Sancus.Configuration.Notification_Channel

    @valid_attrs %{description: "some description", name: "some name", webhook: "some webhook"}
    @update_attrs %{description: "some updated description", name: "some updated name", webhook: "some updated webhook"}
    @invalid_attrs %{description: nil, name: nil, webhook: nil}

    def notification__channel_fixture(attrs \\ %{}) do
      {:ok, notification__channel} =
        attrs
        |> Enum.into(@valid_attrs)
        |> Configuration.create_notification__channel()

      notification__channel
    end

    test "list_notification_channels/0 returns all notification_channels" do
      notification__channel = notification__channel_fixture()
      assert Configuration.list_notification_channels() == [notification__channel]
    end

    test "get_notification__channel!/1 returns the notification__channel with given id" do
      notification__channel = notification__channel_fixture()
      assert Configuration.get_notification__channel!(notification__channel.id) == notification__channel
    end

    test "create_notification__channel/1 with valid data creates a notification__channel" do
      assert {:ok, %Notification_Channel{} = notification__channel} = Configuration.create_notification__channel(@valid_attrs)
      assert notification__channel.description == "some description"
      assert notification__channel.name == "some name"
      assert notification__channel.webhook == "some webhook"
    end

    test "create_notification__channel/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Configuration.create_notification__channel(@invalid_attrs)
    end

    test "update_notification__channel/2 with valid data updates the notification__channel" do
      notification__channel = notification__channel_fixture()
      assert {:ok, %Notification_Channel{} = notification__channel} = Configuration.update_notification__channel(notification__channel, @update_attrs)
      assert notification__channel.description == "some updated description"
      assert notification__channel.name == "some updated name"
      assert notification__channel.webhook == "some updated webhook"
    end

    test "update_notification__channel/2 with invalid data returns error changeset" do
      notification__channel = notification__channel_fixture()
      assert {:error, %Ecto.Changeset{}} = Configuration.update_notification__channel(notification__channel, @invalid_attrs)
      assert notification__channel == Configuration.get_notification__channel!(notification__channel.id)
    end

    test "delete_notification__channel/1 deletes the notification__channel" do
      notification__channel = notification__channel_fixture()
      assert {:ok, %Notification_Channel{}} = Configuration.delete_notification__channel(notification__channel)
      assert_raise Ecto.NoResultsError, fn -> Configuration.get_notification__channel!(notification__channel.id) end
    end

    test "change_notification__channel/1 returns a notification__channel changeset" do
      notification__channel = notification__channel_fixture()
      assert %Ecto.Changeset{} = Configuration.change_notification__channel(notification__channel)
    end
  end

  describe "warningperiods" do
    alias Sancus.Configuration.WarningPeriod

    @valid_attrs %{days: 42}
    @update_attrs %{days: 43}
    @invalid_attrs %{days: nil}

    def warning_period_fixture(attrs \\ %{}) do
      {:ok, warning_period} =
        attrs
        |> Enum.into(@valid_attrs)
        |> Configuration.create_warning_period()

      warning_period
    end

    test "list_warningperiods/0 returns all warningperiods" do
      warning_period = warning_period_fixture()
      assert Configuration.list_warningperiods() == [warning_period]
    end

    test "get_warning_period!/1 returns the warning_period with given id" do
      warning_period = warning_period_fixture()
      assert Configuration.get_warning_period!(warning_period.id) == warning_period
    end

    test "create_warning_period/1 with valid data creates a warning_period" do
      assert {:ok, %WarningPeriod{} = warning_period} = Configuration.create_warning_period(@valid_attrs)
      assert warning_period.days == 42
    end

    test "create_warning_period/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Configuration.create_warning_period(@invalid_attrs)
    end

    test "update_warning_period/2 with valid data updates the warning_period" do
      warning_period = warning_period_fixture()
      assert {:ok, %WarningPeriod{} = warning_period} = Configuration.update_warning_period(warning_period, @update_attrs)
      assert warning_period.days == 43
    end

    test "update_warning_period/2 with invalid data returns error changeset" do
      warning_period = warning_period_fixture()
      assert {:error, %Ecto.Changeset{}} = Configuration.update_warning_period(warning_period, @invalid_attrs)
      assert warning_period == Configuration.get_warning_period!(warning_period.id)
    end

    test "delete_warning_period/1 deletes the warning_period" do
      warning_period = warning_period_fixture()
      assert {:ok, %WarningPeriod{}} = Configuration.delete_warning_period(warning_period)
      assert_raise Ecto.NoResultsError, fn -> Configuration.get_warning_period!(warning_period.id) end
    end

    test "change_warning_period/1 returns a warning_period changeset" do
      warning_period = warning_period_fixture()
      assert %Ecto.Changeset{} = Configuration.change_warning_period(warning_period)
    end
  end

  describe "notificationchannels" do
    alias Sancus.Configuration.NotificationChannel

    @valid_attrs %{description: "some description", name: "some name", webhook: "some webhook"}
    @update_attrs %{description: "some updated description", name: "some updated name", webhook: "some updated webhook"}
    @invalid_attrs %{description: nil, name: nil, webhook: nil}

    def notification_channel_fixture(attrs \\ %{}) do
      {:ok, notification_channel} =
        attrs
        |> Enum.into(@valid_attrs)
        |> Configuration.create_notification_channel()

      notification_channel
    end

    test "list_notificationchannels/0 returns all notificationchannels" do
      notification_channel = notification_channel_fixture()
      assert Configuration.list_notificationchannels() == [notification_channel]
    end

    test "get_notification_channel!/1 returns the notification_channel with given id" do
      notification_channel = notification_channel_fixture()
      assert Configuration.get_notification_channel!(notification_channel.id) == notification_channel
    end

    test "create_notification_channel/1 with valid data creates a notification_channel" do
      assert {:ok, %NotificationChannel{} = notification_channel} = Configuration.create_notification_channel(@valid_attrs)
      assert notification_channel.description == "some description"
      assert notification_channel.name == "some name"
      assert notification_channel.webhook == "some webhook"
    end

    test "create_notification_channel/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Configuration.create_notification_channel(@invalid_attrs)
    end

    test "update_notification_channel/2 with valid data updates the notification_channel" do
      notification_channel = notification_channel_fixture()
      assert {:ok, %NotificationChannel{} = notification_channel} = Configuration.update_notification_channel(notification_channel, @update_attrs)
      assert notification_channel.description == "some updated description"
      assert notification_channel.name == "some updated name"
      assert notification_channel.webhook == "some updated webhook"
    end

    test "update_notification_channel/2 with invalid data returns error changeset" do
      notification_channel = notification_channel_fixture()
      assert {:error, %Ecto.Changeset{}} = Configuration.update_notification_channel(notification_channel, @invalid_attrs)
      assert notification_channel == Configuration.get_notification_channel!(notification_channel.id)
    end

    test "delete_notification_channel/1 deletes the notification_channel" do
      notification_channel = notification_channel_fixture()
      assert {:ok, %NotificationChannel{}} = Configuration.delete_notification_channel(notification_channel)
      assert_raise Ecto.NoResultsError, fn -> Configuration.get_notification_channel!(notification_channel.id) end
    end

    test "change_notification_channel/1 returns a notification_channel changeset" do
      notification_channel = notification_channel_fixture()
      assert %Ecto.Changeset{} = Configuration.change_notification_channel(notification_channel)
    end
  end
end
