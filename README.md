# Sancus

## installation

Copy script `check_ssl_cert` in PATH
Install openssl
Run:

```
mix do deps.get, compile
mix ecto.setup
```

### Slack notifier

Please create an appropriate webhook in Slack.
Ensure the environment variable `SLACK_WEBHOOK` is set by either:

- using a `.env` file at the root of your project (don't commit it!);
- injecting the environment variable in your favorite virtualization tool.

## Domain modelling

The scheduler regularly checks the validity of a list of protected endpoints for a given set of time
and records the last N results.
Should a test fail, the scheduler notifies the recorded notification channels.

Endpoint: identified by a host name and a port
Warning period: the period under which the validity of the certificate is deemed expired in days and triggers a notification
Notification channels: a channel used to notify sysadmins (for now, only slack)
Configuration: the Warning period, a list of Notification channels, a list of Endpoints

## Usage

Ensure file `check_ssl_cert` is in the `$PATH`!
Run `iex -S mix phx.server`
then

```
 {msg, code} = System.cmd("check_ssl_cert", ["--host", "mail.straessle.io", "--port", "993", "--days", "358"])
```

To start your Phoenix server:

- Install dependencies with `mix deps.get`
- Create and migrate your database with `mix ecto.setup`
- Install Node.js dependencies with `cd assets && npm install`
- Start Phoenix endpoint with `mix phx.server`

Now you can visit [`localhost:4000`](http://localhost:4000) from your browser.

Ready to run in production? Please [check our deployment guides](https://hexdocs.pm/phoenix/deployment.html).

## Run with docker-compose

Ensure `.env` is not present (using the `env_file` directive doesn't bypass `.env` file for some reasons...);
Add a `.docker-env` file and fill in the values:

```
SLACK_WEBHOOK=https://hooks.slack.com/services/<SOME_VALID_SLACK_WEBHOOK>
SECRET_KEY_BASE=<SOME_PHOENXI_KEY_BASE>
DATABASE_URL=<SOME_VALID_ECTO_DB_URL_WITHOUT_DOUBLE_QUOTES>
HOSTNAME=<SOME_HOSTNAME>
PORT=4000
MIX_ENV=prod
POOL_SIZE=10
SCHEDULE=@daily
NOTIFY_GOOD_CERTS=true
```

## Run with docker

### Build a test image

`docker build -t sancus:test .`

### Run the docker image

You need to set these environment variables:

```bash
SLACK_WEBHOOK           # a valid slack webhook that you create on Slack to receive notifications
SECRET_KEY_BASE         # a secret you generate from
DATABASE_URL            # an ecto DB string in form: ecto://USER:PASSWORD@HOST/DATABASE"
HOSTNAME                # the hostname of the endpoint, used to create Phoenix URLs
PORT                    # the listening port, defaults to: 4000
MIX_ENV                 # the target environment: prod or dev
POOL_SIZE               # the DB pool size, defaults to "10"
NOTIFY_GOOD_CERTS       # true | false, sends warning for good certificates too, defaults to false
```

And run the docker container:

```
docker run -it --rm \
  -e SLACK_WEBHOOK="https://hooks.slack.com/services/SOME_VALID_SLACK_WEBHOOK" \
  -e DATABASE_URL="SOME_VALID_ECTO_DATABASE_URL" \
  -e SECRET_KEY_BASE="SOME_SECRET_KEY_BASE" \
  -e HOSTNAME="SOME_HOSTNAME" \
  -e PORT="SOME_PORT" \
  -e MIX_ENV="prod" \
  -e POOL_SIZE="10" \
  -e NOTIFY_GOOD_CERTS="true" \
  -p 4000:4000 \
  --name sancus_test \
  sancus:test
```

### Run the migrations

Make sure the database exists already.
Also, the migration doesn't have seed data, so you need to add some endpoints and warning_period to check.

```
docker exec sancus_test sancus eval "Sancus.Release.migrate"
```

## Helm Chart

### Documentation

- https://github.com/phcollignon
- https://app.pluralsight.com/library/courses/packaging-applications-helm-kubernetes/table-of-contents
