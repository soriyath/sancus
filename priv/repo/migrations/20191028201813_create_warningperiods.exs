defmodule Sancus.Repo.Migrations.CreateWarningperiods do
  use Ecto.Migration

  def change do
    create table(:warningperiods) do
      add :days, :integer

      timestamps()
    end

  end
end
