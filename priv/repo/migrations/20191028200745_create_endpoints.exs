defmodule Sancus.Repo.Migrations.CreateEndpoints do
  use Ecto.Migration

  def change do
    create table(:endpoints) do
      add :host, :text
      add :port, :text
      add :description, :text

      timestamps()
    end
  end
end
