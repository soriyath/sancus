defmodule Sancus.Repo.Migrations.CreateNotificationchannels do
  use Ecto.Migration

  def change do
    create table(:notificationchannels) do
      add :name, :text
      add :description, :text
      add :webhook, :text

      timestamps()
    end

  end
end
