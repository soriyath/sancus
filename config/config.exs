# This file is responsible for configuring your application
# and its dependencies with the aid of the Config module.
#
# This configuration file is loaded before any dependency and
# is restricted to this project.

# General application configuration
import Config

config :sancus,
  ecto_repos: [Sancus.Repo]

# Configures the endpoint
config :sancus, SancusWeb.Endpoint,
  url: [host: "localhost"],
  secret_key_base: "HCrGsa3LlSNZRZ3Wh7iomqTEQi6xPR/6TApTx6XVHfhAsM71WkCJ0evyfsmSfZhW",
  render_errors: [view: SancusWeb.ErrorView, accepts: ~w(html json)],
  pubsub: [name: Sancus.PubSub, adapter: Phoenix.PubSub.PG2]

# Configures Elixir's Logger
config :logger, :console,
  format: "$time $metadata[$level] $message\n",
  metadata: [:request_id]

# Use Jason for JSON parsing in Phoenix
config :phoenix, :json_library, Jason

# Import environment specific config. This must remain at the bottom
# of this file so it overrides the configuration defined above.
import_config "#{Mix.env()}.exs"
