defmodule SancusWeb.PageController do
  use SancusWeb, :controller

  def index(conn, _params) do
    render(conn, "index.html")
  end
end
