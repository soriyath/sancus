defmodule Sancus do
  @moduledoc """
  Sancus keeps the contexts that define your domain
  and business logic.

  Contexts are also responsible for managing your data, regardless
  if it comes from the database, an external API or others.
  """

  alias Algae.Either
  alias Either.{Left, Right}
  alias Sancus.Configuration

  def check_ssl_certificates do
    with endpoints <- Configuration.list_endpoints(),
         warning_periods <- Configuration.list_warning_periods(),
         notify_good_certs <- System.get_env("NOTIFY_GOOD_CERTS") do
      warning_periods
      |> List.first()
      |> do_check_ssl_certificates(notify_good_certs, endpoints, [])
    end
  end

  defp do_check_ssl_certificates(warning_period, notify_good_certs, input, output) do
    case input do
      [] ->
        output

      [endpoint | rest] ->
        IO.puts("Testing host #{endpoint.host} on port #{endpoint.port}")

        {_msg, code} =
          System.cmd("check_ssl_cert", [
            "--host",
            endpoint.host,
            "--port",
            endpoint.port,
            "--days",
            Integer.to_string(warning_period.days)
          ])

        case code do
          1 ->
            IO.puts(
              "The SSL certificate of host #{endpoint.host} on port #{endpoint.port} will expire within the next #{
                warning_period.days
              } days."
            )

            Sancus.Configuration.NotificationChannel.Slack.send_message(
              "The SSL certificate of host #{endpoint.host} on port #{endpoint.port} will expire within the next #{
                warning_period.days
              } days."
            )

          0 ->
            IO.puts(
              "The SSL certificate of host #{endpoint.host} on port #{endpoint.port} is still valid for the next #{
                warning_period.days
              } days."
            )

            case notify_good_certs do
              "true" ->
                Sancus.Configuration.NotificationChannel.Slack.send_message(
                  "The SSL certificate of host #{endpoint.host} on port #{endpoint.port} is still valid for the next #{
                    warning_period.days
                  } days."
                )

              _ ->
                nil
            end

          _ ->
            nil
        end

        do_check_ssl_certificates(warning_period, notify_good_certs, rest, [
          %{endpoint: endpoint, warning_period: warning_period, result: code} | output
        ])
    end
  end
end
