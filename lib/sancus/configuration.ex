defmodule Sancus.Configuration do
  @moduledoc """
  The Configuration context.
  """

  import Ecto.Query, warn: false
  alias Sancus.Repo

  alias Sancus.Configuration.Endpoint

  @doc """
  Returns the list of endpoints.

  ## Examples

      iex> list_endpoints()
      [%Endpoint{}, ...]

  """
  def list_endpoints do
    Repo.all(Endpoint)
  end

  @doc """
  Gets a single endpoint.

  Raises `Ecto.NoResultsError` if the Endpoint does not exist.

  ## Examples

      iex> get_endpoint!(123)
      %Endpoint{}

      iex> get_endpoint!(456)
      ** (Ecto.NoResultsError)

  """
  def get_endpoint!(id), do: Repo.get!(Endpoint, id)

  @doc """
  Creates a endpoint.

  ## Examples

      iex> create_endpoint(%{field: value})
      {:ok, %Endpoint{}}

      iex> create_endpoint(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def create_endpoint(attrs \\ %{}) do
    %Endpoint{}
    |> Endpoint.changeset(attrs)
    |> Repo.insert()
  end

  @doc """
  Updates a endpoint.

  ## Examples

      iex> update_endpoint(endpoint, %{field: new_value})
      {:ok, %Endpoint{}}

      iex> update_endpoint(endpoint, %{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def update_endpoint(%Endpoint{} = endpoint, attrs) do
    endpoint
    |> Endpoint.changeset(attrs)
    |> Repo.update()
  end

  @doc """
  Deletes a Endpoint.

  ## Examples

      iex> delete_endpoint(endpoint)
      {:ok, %Endpoint{}}

      iex> delete_endpoint(endpoint)
      {:error, %Ecto.Changeset{}}

  """
  def delete_endpoint(%Endpoint{} = endpoint) do
    Repo.delete(endpoint)
  end

  @doc """
  Returns an `%Ecto.Changeset{}` for tracking endpoint changes.

  ## Examples

      iex> change_endpoint(endpoint)
      %Ecto.Changeset{source: %Endpoint{}}

  """
  def change_endpoint(%Endpoint{} = endpoint) do
    Endpoint.changeset(endpoint, %{})
  end

  alias Sancus.Configuration.WarningPeriod

  @doc """
  Returns the list of warningperiods.

  ## Examples

      iex> list_warning_periods()
      [%WarningPeriod{}, ...]

  """
  def list_warning_periods do
    Repo.all(WarningPeriod)
  end

  @doc """
  Gets a single warning_period.

  Raises `Ecto.NoResultsError` if the Warning period does not exist.

  ## Examples

      iex> get_warning_period!(123)
      %WarningPeriod{}

      iex> get_warning_period!(456)
      ** (Ecto.NoResultsError)

  """
  def get_warning_period!(id), do: Repo.get!(WarningPeriod, id)

  @doc """
  Creates a warning_period.

  ## Examples

      iex> create_warning_period(%{field: value})
      {:ok, %WarningPeriod{}}

      iex> create_warning_period(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def create_warning_period(attrs \\ %{}) do
    %WarningPeriod{}
    |> WarningPeriod.changeset(attrs)
    |> Repo.insert()
  end

  @doc """
  Updates a warning_period.

  ## Examples

      iex> update_warning_period(warning_period, %{field: new_value})
      {:ok, %WarningPeriod{}}

      iex> update_warning_period(warning_period, %{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def update_warning_period(%WarningPeriod{} = warning_period, attrs) do
    warning_period
    |> WarningPeriod.changeset(attrs)
    |> Repo.update()
  end

  @doc """
  Deletes a WarningPeriod.

  ## Examples

      iex> delete_warning_period(warning_period)
      {:ok, %WarningPeriod{}}

      iex> delete_warning_period(warning_period)
      {:error, %Ecto.Changeset{}}

  """
  def delete_warning_period(%WarningPeriod{} = warning_period) do
    Repo.delete(warning_period)
  end

  @doc """
  Returns an `%Ecto.Changeset{}` for tracking warning_period changes.

  ## Examples

      iex> change_warning_period(warning_period)
      %Ecto.Changeset{source: %WarningPeriod{}}

  """
  def change_warning_period(%WarningPeriod{} = warning_period) do
    WarningPeriod.changeset(warning_period, %{})
  end

  alias Sancus.Configuration.NotificationChannel

  @doc """
  Returns the list of notificationchannels.

  ## Examples

      iex> list_notification_channels()
      [%NotificationChannel{}, ...]

  """
  def list_notification_channels do
    Repo.all(NotificationChannel)
  end

  @doc """
  Gets a single notification_channel.

  Raises `Ecto.NoResultsError` if the Notification channel does not exist.

  ## Examples

      iex> get_notification_channel!(123)
      %NotificationChannel{}

      iex> get_notification_channel!(456)
      ** (Ecto.NoResultsError)

  """
  def get_notification_channel!(id), do: Repo.get!(NotificationChannel, id)

  @doc """
  Creates a notification_channel.

  ## Examples

      iex> create_notification_channel(%{field: value})
      {:ok, %NotificationChannel{}}

      iex> create_notification_channel(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def create_notification_channel(attrs \\ %{}) do
    %NotificationChannel{}
    |> NotificationChannel.changeset(attrs)
    |> Repo.insert()
  end

  @doc """
  Updates a notification_channel.

  ## Examples

      iex> update_notification_channel(notification_channel, %{field: new_value})
      {:ok, %NotificationChannel{}}

      iex> update_notification_channel(notification_channel, %{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def update_notification_channel(%NotificationChannel{} = notification_channel, attrs) do
    notification_channel
    |> NotificationChannel.changeset(attrs)
    |> Repo.update()
  end

  @doc """
  Deletes a NotificationChannel.

  ## Examples

      iex> delete_notification_channel(notification_channel)
      {:ok, %NotificationChannel{}}

      iex> delete_notification_channel(notification_channel)
      {:error, %Ecto.Changeset{}}

  """
  def delete_notification_channel(%NotificationChannel{} = notification_channel) do
    Repo.delete(notification_channel)
  end

  @doc """
  Returns an `%Ecto.Changeset{}` for tracking notification_channel changes.

  ## Examples

      iex> change_notification_channel(notification_channel)
      %Ecto.Changeset{source: %NotificationChannel{}}

  """
  def change_notification_channel(%NotificationChannel{} = notification_channel) do
    NotificationChannel.changeset(notification_channel, %{})
  end
end
