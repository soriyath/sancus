defmodule Sancus.Configuration.WarningPeriod do
  use Ecto.Schema
  import Ecto.Changeset

  schema "warningperiods" do
    field :days, :integer

    timestamps()
  end

  @required_fields ~w(days)a

  @doc false
  def changeset(warning_period, attrs) do
    warning_period
    |> cast(attrs, @required_fields)
    |> validate_required(@required_fields)
  end
end
