defmodule Sancus.Configuration.Scheduler do
  use Quantum.Scheduler,
    otp_app: :sancus

  import Algae
  alias Quantum.Job

  def initialize do
    with env <- System.get_env("MIX_ENV") do
      case env do
        "prod" ->
          do_initialize()

        _ ->
          %Algae.Either.Left{
            left:
              "Sancus.Configuration.Scheduler is configured with static configuration for environment #{
                env
              }"
          }
      end
    end
  end

  defp do_initialize do
    IO.puts("Initializing Quantum jobs...")

    schedule =
      System.get_env("SCHEDULE")
      |> Crontab.CronExpression.Parser.parse!()

    __MODULE__.new_job()
    |> Job.set_name(:sancus_scheduler)
    |> Job.set_schedule(schedule)
    |> Job.set_task(fn -> Sancus.check_ssl_certificates() end)
    |> __MODULE__.add_job()
  end
end
