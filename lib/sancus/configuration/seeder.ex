defmodule Sancus.Configuration.Seeder do
  @moduledoc """
  Seeder parses a CSV file to seed the database
  it should match one of these pattern:

  endpoints, description, host, port
  notificationchannels, description, name, webhook
  warningperiods, days
  """
  import Algae
  alias Algae.Either.Left
  alias Sancus.Configuration.{Endpoint, NotificationChannel, WarningPeriod}

  @app :sancus

  def seed(absolute_path_to_file) do
    with _repos <- repos() do
      Ecto.Migrator.with_repo(Sancus.Repo, fn _ ->
        absolute_path_to_file
        |> File.stream!()
        |> CSV.decode(headers: true)
        |> Enum.map(&parse(&1))
      end)
    end
  end

  defp parse(entry) do
    case entry do
      {:ok, %{"description" => d, "name" => n, "webhook" => h}} ->
        Sancus.Repo.insert!(%NotificationChannel{description: d, name: n, webhook: h})

      {:ok, %{"description" => d, "host" => h, "port" => p}} ->
        Sancus.Repo.insert!(%Endpoint{description: d, host: h, port: p})

      {:ok, %{"days" => d}} ->
        Sancus.Repo.insert!(%WarningPeriod{days: String.to_integer(d)})

      {:error, reason} ->
        %Left{left: reason}
    end
  end

  defp repos do
    Application.load(@app)
    Application.fetch_env!(@app, :ecto_repos)
  end
end
