defmodule Sancus.Configuration.NotificationChannel.Slack do
  import Algae
  alias Algae.Either
  alias Either.{Left, Right}

  @headers [{"Content-type", "application/json"}]

  def send_message(text) when is_binary(text) do
    text
    |> prepare_message
    |> do_send_message
  end

  def send_message(_) do
    %Left{left: "Unsupported or no argument passed to Slack.send_message"}
  end

  defp do_send_message(either) do
    webhook = System.get_env("SLACK_WEBHOOK")

    response =
      case either do
        %Right{right: body} ->
          case HTTPoison.post(webhook, body, @headers) do
            {:ok, response} -> %Right{right: response}
            {:error, reason} -> %Left{left: reason}
          end

        %Left{left: reason} ->
          %Left{left: reason}

        _ ->
          %Left{left: "Unknown error has happend in Slack.do_send_message(%Either.#{either})"}
      end

    response
  end

  defp prepare_message(text) when is_binary(text) do
    case Poison.encode(%{text: text}) do
      {:ok, body} -> %Right{right: body}
      {:error, %Poison.EncodeError{message: reason, value: _}} -> %Left{left: reason}
      {:error, _} -> %Left{left: "Unknown error happened in Slack.prepare_messag(#{text})"}
    end
  end

  defp prepare_message(_),
    do: %Left{left: "Unsupported or no argument passed to Slack.prepare_message"}

  defp prepare_message(),
    do: %Left{left: "Unsupported or no argument passed to Slack.prepare_message"}
end
