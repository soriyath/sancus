defmodule Sancus.Configuration.Endpoint do
  use Ecto.Schema
  import Ecto.Changeset

  schema "endpoints" do
    field :description, :string
    field :host, :string
    field :port, :string

    timestamps()
  end

  @required_fields ~w(host)a
  @optional_fields ~w(description port)a

  @doc false
  def changeset(endpoint, attrs) do
    endpoint
    |> cast(attrs, @required_fields ++ @optional_fields)
    |> validate_required(@required_fields)
  end
end
