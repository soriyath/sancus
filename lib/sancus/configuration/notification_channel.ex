defmodule Sancus.Configuration.NotificationChannel do
  use Ecto.Schema
  import Ecto.Changeset

  schema "notificationchannels" do
    field :description, :string
    field :name, :string
    field :webhook, :string

    timestamps()
  end

  @required_fields ~w(webhook)a
  @optional_fields ~w(description name)a

  @doc false
  def changeset(notification_channel, attrs) do
    notification_channel
    |> cast(attrs, @required_fields ++ @optional_fields)
    |> validate_required(@required_fields)
  end
end
