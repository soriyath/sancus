defmodule Sancus.Repo do
  use Ecto.Repo,
    otp_app: :sancus,
    adapter: Ecto.Adapters.Postgres
end
