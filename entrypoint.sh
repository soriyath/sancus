#!/usr/bin/env sh

echo "Running Sancus migrations..."
sancus eval "Sancus.Release.migrate"
echo ""

echo "Seeding Sancus database..."
if [ -d "/tmp/sancus" ]; then
  for file in $(ls /tmp/sancus/*.csv); do
    echo "Treating file: $file"
    sancus eval "Sancus.Configuration.Seeder.seed(\"$file\")"
  done
else 
  echo "/tmp/sancus does not exist. mount the volume with CSV files to seed the DB."
fi
echo ""

echo "Starting Sancus app..."
sancus start
