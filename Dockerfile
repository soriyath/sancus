###################
### Build stage ###
###################

FROM elixir:1.9.0-alpine as build
ENV APP_NAME sancus

# install build dependencies
RUN apk add --update git build-base nodejs npm yarn python

ENV MIX_ENV=prod
WORKDIR /usr/local/src/${APP_NAME}

RUN mix local.hex --force && \
  mix local.rebar --force

# install mix dependencies
COPY mix.exs mix.lock ./
COPY config config
RUN mix do deps.get, compile

# build assets
COPY assets assets
RUN cd assets && npm install && npm run deploy
RUN mix phx.digest

# build project
COPY priv priv
COPY lib lib
RUN mix compile

# build release
RUN mix release

#####################
### Release image ###
#####################

FROM alpine:3.9
ENV APP_NAME sancus
RUN apk add --update bash libressl fish && \
  mkdir -p /usr/local/src/${APP_NAME}
WORKDIR /usr/local/src/${APP_NAME}
COPY --from=build /usr/local/src/${APP_NAME}/_build/prod/rel/sancus ./
COPY check_ssl_cert /usr/local/bin/
COPY entrypoint.sh /usr/local/src/${APP_NAME}/
RUN chown -R nobody: /usr/local/src/${APP_NAME} && \
  chmod +x /usr/local/src/${APP_NAME}/entrypoint.sh && \
  ln -s /usr/local/src/${APP_NAME}/bin/${APP_NAME} /usr/local/bin/${APP_NAME}
USER nobody
ENV HOME=/usr/local/src/${APP_NAME}
ENV SANCUS_PATH=/usr/local/src/${APP_NAME}

EXPOSE 4000
ENTRYPOINT [ "/usr/local/src/sancus/entrypoint.sh" ]
